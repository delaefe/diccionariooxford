<%-- 
    Document   : index
    Created on : 06-05-2021, 17:09:48
    Author     : delaefe
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Index</title>
    </head>
    <body>
        <div class="container-fluid">
            <h1>Diccionario Oxford</h1>


                <form name="form" action="controller" method="POST">

                    <label for="palabra">Palabra</label>
                    <input name="palabra" type="text" ></input><br> 

                    <button class="btn btn-success" type="submit" name="action" value="ingreso">Consultar</button>
                    <button class="btn btn-primary" type="submit" name="action" value="historial">Ver lista de consultas</button>

                </form>
        </div><!-- comment -->
        
        <hr><br>
        <footer>
            Diego de la Fuente Curaqueo
        </footer>
    </body>
</html>
