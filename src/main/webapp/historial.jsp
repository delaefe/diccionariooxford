<%-- 
    Document   : historial
    Created on : 07-05-2021, 15:49:02
    Author     : delaefe
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="entity.diccionarooxford.Palabras"%>

 <!DOCTYPE html>
<%
    List<Palabras> pal = (List<Palabras>) request.getAttribute("historialPalabras");
    Iterator<Palabras> itPal = pal.iterator();
%>

<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Historial</title>
    </head>
    <body>
        
        <div class="container-fluid">
        
        <form name="form" action="controller" method="POST">
           <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">  
               <h1> Historial de consultas </h1>
               <table border="1">
                    <thead>
                        <th> Palabra </th>
                        <th> Significado</th>
                        <th> Fecha Búsqueda</th>
                     </thead>
                    
                    <tbody>
                        <%while (itPal.hasNext()) {
                        Palabras per = itPal.next();%>
                        <tr>
                            <td><%= per.getPalabra()%></td>
                            <td><%= per.getSignificado()%></td>
                            <td><%= per.getFechabusqueda()%></td>

                        </tr>
                        <%}%>           
                    </tbody>           
                </table>
            <br>        
            <button class="btn btn-success" type="submit" name="action" value="consultar">Consultar palabra</button>
            
       </form>  
           
        </div><!-- comment -->
                        
                        
        <hr><br>
        <footer>
            Diego de la Fuente Curaqueo
        </footer>            
                    
    </body>
</html>
