<%-- 
    Document   : definicion
    Created on : 07-05-2021, 22:22:50
    Author     : delaefe
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entity.diccionarooxford.Palabras"%>
<%
    Palabras pal = (Palabras) request.getAttribute("laPalabraBuscada");
 %>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Definición</title>
    </head>
    <body>
        
        <div class="container-fluid">

            <h1> <%= pal.getPalabra()%>  </h1>

            <p> <%= pal.getSignificado()%> </p>


            <br><br>

                <form name="form" action="controller" method="POST">

                    <button class="btn btn-success" type="submit" name="action" value="consultar">Consultar otra palabra</button>
                    <button class="btn btn-primary" type="submit" name="action" value="historial">Ver lista de consultas</button>

                </form>
        </div><!-- comment -->
        
        <hr><br>
        <footer>
            Diego de la Fuente Curaqueo
        </footer>
    </body>
</html>
