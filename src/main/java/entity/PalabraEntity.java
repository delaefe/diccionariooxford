/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author delaefe
 */
public class PalabraEntity {
    private String termino;
    private String significado;
    private String fecha;

    /**
     * @return the termino
     */
    public String getTermino() {
        return termino;
    }

    /**
     * @return the significado
     */
    public String getSignificado() {
        return significado;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param termino the termino to set
     */
    public void setTermino(String termino) {
        this.termino = termino;
    }

    /**
     * @param significado the significado to set
     */
    public void setSignificado(String significado) {
        this.significado = significado;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    
}
