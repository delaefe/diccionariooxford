/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.diccionarooxford;

import dto.PalabraDTO;
import entity.PalabraEntity;
import java.util.ArrayList;
import java.util.Date;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
 
/**
 *
 * @author delaefe
 */
@Path("diccionario")
public class ApiDiccionarioInterno {
   
    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response definir( @PathParam("idbuscar") String idbuscar){
        
        Client cliente = ClientBuilder.newClient();
        
        WebTarget miRecurso = cliente.target("https://od-api.oxforddictionaries.com/api/v2/entries/es/"+idbuscar+"?fields=definitions");
        
        PalabraDTO palabraConsultada = miRecurso.request(MediaType.APPLICATION_JSON).header("app_key","8e7326d644e310a24fb2390bce1f5297").header("app_id", "fa3aa2f5").get( PalabraDTO.class );
        
        PalabraEntity palabra = new PalabraEntity();
        
        palabra.setTermino(palabraConsultada.getWord() );
        
        Date fecha = new Date();
        palabra.setFecha( fecha.toString() );
        palabra.setSignificado(cortaString( (String) palabraConsultada.getResults().get(0).toString() ) );

        //String definicion = palabraConsultada.getResults().get(0).getLexicalEntries;
        //palabra.setSignificado(definicion);
        
        //System.out.println(definicion);

        //palabra.setSignificado( palabraConsultada.getResults(). );
        //JsonObject obj = jsonReader.readObject( PalabraDTO.getResults().get(0) );
        //System.out.println (obj.getJSONObject("lexicalEntries").toString() );
        //String definition = obj.getJSONObject("lexicalEntries").getString("definitions");
        //System.out.println(PalabraDTO.getResults().get(0));
        
        return Response.ok(200).entity(palabra).build();
    }
    
     public static String cortaString (String str){
        
        String cadena = str.substring( str.indexOf("definitions=[")+13 , str.length() );
        cadena = cadena.substring(0, cadena.indexOf("]}") );

        return cadena+".";

    }
}
