/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import dao.diccionarooxford.PalabrasJpaController;
import entity.diccionarooxford.Palabras;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author delaefe
 */
@WebServlet(name = "controller", urlPatterns = {"/controller"})
public class controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet controller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //valor de botón presionado
        String boton = request.getParameter("action");

        //dao
        PalabrasJpaController dao = new PalabrasJpaController();
        
        //ingresa nueva palabra para consultar (redirige a definicion.jsp) - - - - - - - - - - -
        if(boton.equals("ingreso")){
            
            //palabra a buscar
            String palabra  = request.getParameter("palabra");
            
            
            //llamamos a nuestra propia api para que le consulte a los de oxford
            Client client = ClientBuilder.newClient();
            WebTarget myResource = client.target("https://diccionariooxford.herokuapp.com/api/diccionario/" + palabra);

            
            //extaemos y casteamos la definicion
            Palabras definicion = (Palabras) myResource.request(MediaType.APPLICATION_JSON).get( new GenericType<Palabras>() { } );
            
            
            System.out.println(definicion.getSignificado());
           
            //creamos nueva instancia y asignamos valores.
            Palabras palabraBuscada = new Palabras();
            palabraBuscada.setPalabra( palabra );
            palabraBuscada.setId( palabra );
            palabraBuscada.setSignificado(  definicion.getSignificado() );
            
            //agregamos la fecha de la búsqueda
            Date fechaDeBusqueda = new Date();
            palabraBuscada.setFechabusqueda(fechaDeBusqueda.toString());
            
            //agregamos elemento a la bd interna
            try {
                dao.create( palabraBuscada ); 
            } catch (Exception ex) {
                Logger.getLogger(controller.class.getName()).log(Level.SEVERE, null, ex);
            }
 
            request.setAttribute("laPalabraBuscada",palabraBuscada);
            request.getRequestDispatcher("definicion.jsp").forward(request, response);

        //ver historial (redirecciona a historial.jsp)  - - - - - - - - - - - - - - - - - - - - - 
        }else if( boton.equals("historial") ){
            
            List<Palabras> listaPalabras = dao.findPalabrasEntities();
            request.setAttribute("historialPalabras", listaPalabras);
            request.getRequestDispatcher("historial.jsp").forward(request, response);

        //volver a consultar una palabra (redirecciona a index.jsp) - - - - - - - - - - - - - - -
        }else if (boton.equals("consultar")){
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
