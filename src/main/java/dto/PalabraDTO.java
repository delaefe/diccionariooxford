/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.util.ArrayList;

/**
 *
 * @author delaefe
 */
public class  PalabraDTO {
  private String id;
  Metadata MetadataObject;
  private ArrayList<Object> results = new ArrayList<Object>();
  private String word;


 // Getter Methods 

  public String getId() {
    return id;
  }

  public Metadata getMetadata() {
    return MetadataObject;
  }

  public String getWord() {
    return word;
  }

 // Setter Methods 

  public void setId( String id ) {
    this.id = id;
  }

  public void setMetadata( Metadata metadataObject ) {
    this.MetadataObject = metadataObject;
  }

  public void setWord( String word ) {
    this.word = word;
  }

    /**
     * @return the results
     */
    public ArrayList<Object> getResults() {
        return this.results;
    }

    /**
     * @param results the results to set
     */
    public void setResults(ArrayList<Object> results) {
        this.results = results;
    }
}

